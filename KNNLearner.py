#!/usr/local/bin/python

import numpy as np

class KNNLearner:
	def __init__(self, k):
		self.k = k

	def addEvidence(self, Xtrain, Ytrain):
		self.Xtrain = Xtrain
		self.Ytrain = Ytrain

	def query(self, Xtest):
		Y = np.zeros(len(Xtest))
		for i in range(0, len(Xtest)):
			dist = np.zeros(len(self.Xtrain))
			for j in range(0, len(self.Xtrain)):
					dist[j] = np.linalg.norm(self.Xtrain[j] - Xtest[i]) 
			selected  =  self.Ytrain[np.argsort(dist)[0:self.k]]
			Y[i] = np.mean(selected)

		return Y