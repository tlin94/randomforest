#!/usr/local/bin/python
import numpy as np
from scipy import stats

class LinRegLearner:
	
	def __init__(self):
		self.Xtrain = None
		self.Ytrain = None
		self.w = None
		self.res = None

	def addEvidence(self, Xtrain, Ytrain):
		self.Xtrain = Xtrain
		self.Ytrain = Ytrain

		A = np.hstack((self.Xtrain, np.ones((len(self.Xtrain), 1))))
		coeffs = np.linalg.lstsq(A, Ytrain)[0]
		self.w = np.zeros(2)
		self.w[0] = coeffs[0]
		self.w[1] = coeffs[1]
		self.res = coeffs[2]

	def query(self, Xtest):
		Y = np.dot(Xtest, self.w) + self.res
		return Y