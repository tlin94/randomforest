#!/usr/local/bin/python

import numpy as np
import matplotlib.pyplot as plt
import csv
import KNNLearner as knn
import RandomForestLearner as rfl

def readData (filename):
	reader = csv.reader(open(filename, 'rU'), delimiter=',')
	totalRows = len(open(filename).readlines())
	trainRows = int(totalRows * 0.6)
	count = 0
	Xtrain = []; Ytrain = [];
	Xtest = []; Ytest = [];
	for row in reader:
		if count < trainRows:
			Xtrain.append([float(row[0]),float(row[1])])
			Ytrain.append(float(row[2]))
			count +=1
		else:
			Xtest.append([float(row[0]),float(row[1])]) 
			Ytest.append(float(row[2]))
			count +=1

	Xtrain = np.array(Xtrain); Ytrain = np.array(Ytrain)
	Xtest = np.array(Xtest); Ytest = np.array(Ytest)

	return [Xtrain, Ytrain, Xtest, Ytest]



def calculateRMS (predict, actual):
	return np.linalg.norm(predict - actual) / np.sqrt(len(predict))


if __name__ == '__main__':

	files =["data-classification-prob", "data-ripple-prob"]
	kRange = 101;
	for filename in files:
		print "Using dataset %s" %filename
		[Xtrain, Ytrain, Xtest, Ytest] = readData(filename + '.csv')	
		
		#================= test RandomForest =================
		print "test RandomForest learner..."
		k = range(1,kRange)
		array_rms_out = [];
		array_rms_in = [];
		array_rms_out_knn = [];
		for i in k:
			learner = rfl.RandomForestLearner(k=i)
			learner.addEvidence(Xtrain, Ytrain)
			Y_out = learner.query(Xtest)
			rms_out = calculateRMS(Y_out, Ytest)
			array_rms_out.append(rms_out)
			Y_in= learner.query(Xtrain)
			rms_in = calculateRMS(Y_in, Ytrain)
			array_rms_in.append(rms_in)

			knnLearner = knn.KNNLearner(i)
			knnLearner.addEvidence(Xtrain, Ytrain)
			Y_out_knn = knnLearner.query(Xtest)
			rms_out_knn = calculateRMS(Y_out_knn, Ytest)
			array_rms_out_knn.append(rms_out_knn)
			
		
		minValue = min(array_rms_out)
		minIdx = k[array_rms_out.index(minValue)]
		print "Best K for out of sample: %d, minimum RMS: %f" %(minIdx, minValue)
		fig = plt.figure()
		plt.clf()
		plt.plot(k, array_rms_out,'r',k,array_rms_in,'b')
		plt.title('random forest_%s' %filename)
		plt.legend(['out of sample','in sample'])
		plt.xlabel('k')
		plt.ylabel('rms')
		plt.savefig('rms_randomforest_%s' %filename)

		fig = plt.figure()
		plt.clf()
		plt.plot(k, array_rms_out,'r',k,array_rms_out_knn,'b')
		plt.title('random forest v.s knn_%s' %filename)
		plt.legend(['Random Forest','KNN'])
		plt.xlabel('k')
		plt.ylabel('rms')
		plt.savefig('randomforest vs knn_%s' %filename)

	plt.show()