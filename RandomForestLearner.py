#!/usr/local/bin/python
import numpy as np
import random
import csv as csv
import copy

class Node:
    def __init__(self, feature, split, leftNode, rightNode, y):
        self.feature = feature
        self.splitValue = split
        self.left = leftNode
        self.right = rightNode
        self.y = y


class DescisionTreeLearner:
	def __init__(self):
		self.tree = None
		# self.Xtrain = None
		# self.Ytrain = None

	def buildTree(self, Xtrain, Ytrain, featureSet): 
		
		if(len(Ytrain) == 1):
			root = Node(-1, -1, None, None, Ytrain[0])
			
		else:
			feature = random.choice(featureSet)
			splitValue = np.mean(random.sample(Xtrain[:,feature],2))
			y = np.mean(Ytrain)
			leftXtrain = np.empty((0,Xtrain.shape[1]),float)
			leftYtrain = np.array([])
			rightXtrain = np.empty((0,Xtrain.shape[1]),float)
			rightYtrain = np.array([])
			for i in range(0,len(Xtrain)):
				if(Xtrain[i,feature]< splitValue):
					leftXtrain = np.vstack((leftXtrain,Xtrain[i]))
					leftYtrain = np.append(leftYtrain, Ytrain[i])
				else:
					rightXtrain = np.vstack((rightXtrain,Xtrain[i]))
					rightYtrain = np.append(rightYtrain, Ytrain[i])

			left = self.buildTree(leftXtrain, leftYtrain, featureSet)
			right = self.buildTree(rightXtrain, rightYtrain, featureSet)
	
			root = Node(feature, splitValue, left, right, y)

		return root


	def addEvidence(self, Xtrain, Ytrain):
		featureSet = range(Xtrain.shape[1])
		self.tree = self.buildTree(Xtrain, Ytrain, featureSet)

        
	def search(self, x):
		node = self.tree
		while(node.feature != -1):
			if(x[node.feature] < node.splitValue):
				node = node.left
			else:
				node = node.right

		return node.y
		

	def query(self, Xtest):
		Ytest = np.zeros(Xtest.shape[0])
		for i in range(0, len(Xtest)):
			Ytest[i] = self.search(Xtest[i])

		return Ytest

	
class RandomForestLearner:
	def __init__(self, k=1):
		self.trees = None
		self.k = k
		# self.Xtrain = None
		# self.Ytrain = None

	def getShuffledTrainData(self, Xtrain, Ytrain):
		tmpIdx = range(600)
		np.random.shuffle(tmpIdx)
		return [Xtrain[tmpIdx[0:360]], Ytrain[tmpIdx[0:360]]]

	def addEvidence(self, Xtrain, Ytrain):
		self.trees = list()
		for i in range(0, self.k):
			learner = DescisionTreeLearner()
			[Xtrain_, Ytrain_] = self.getShuffledTrainData(Xtrain, Ytrain)
			learner.addEvidence(Xtrain_,Ytrain_)
			self.trees.append(learner)

	def query(self, Xtest): 
		Ytest = np.zeros(Xtest.shape[0])
		for i in range(0, len(Xtest)):
			results = np.zeros(self.k)
			for j in range(0, self.k):
				learner = self.trees[j]
				results[j] = learner.query(np.array([Xtest[i]]))

			Ytest[i] = np.mean(results) 

		return Ytest

